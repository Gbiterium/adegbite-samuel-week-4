<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
class ApiController extends Controller
{
    public function create(Request $request){
        $student = new Student();
        $student->firstName = $request->input('firstName');
        $student->lastName = $request->input('lastName');
        $student->email = $request->input('email');

        $student->save();
        return response()->json($student);
    }

    public function read(){
        $student = Student::all();
        return response()->json($student);
    }

    public function readbyid($id){
        $student = Student::find($id);
        return response()->json($student);
    }

    public function updatebyid(Request $request, $id){
       $student = Student::find($id);
       $student->firstName = $request->input('firstName');
       $student->lastName = $request->input('lastName');
       $student->email = $request->input('email');

       $student->save();
       return response()->json($student);
    }
    public function deletebyid(Request $request, $id){
        $student = Student::find($id);

        $student->delete();
        return response()->json($student);
    }
}
