<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'api';
    protected $fillable = ['firstName','lastName','email'];
}
